package com.tutorialspoint;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "patient")
@XmlType(propOrder={"id", "name", "gender", "age"})
public class Patient extends User {
	
	private int id;
	private String name;
	private String gender;
	private int age;
	private AppointmentList appointmentList;
	
	public Patient(){}
	
	public Patient(int id, String name, String gender, int age){
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.age = age;
		appointmentList = new AppointmentList();
	}
	
	@XmlElement
	public int getId(){
		return this.id;
	}
	
	@XmlElement
	public String getName(){
		return this.name;
	}
	
	@XmlElement
	public String getGender(){
		return this.gender;
	}
	
	@XmlElement
	public int getAge(){
		return this.age;
	}
	
	public AppointmentList getAppointmentList(){
		return this.appointmentList;
	}
	
	public void setAppointment(String date, String person){
		appointmentList.addAppointment(date, person);
	}
	
	public boolean equals(Object object){
		if(object == null){
			return false;
		}else if(!(object instanceof Patient)){
			return false;
		}else{
			Patient patient = (Patient)object;
			if(id == patient.getId() && name == patient.getName()){
				return true;
			}
			return false;
		}
	}

}
