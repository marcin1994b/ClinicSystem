package com.tutorialspoint;

import javax.xml.bind.annotation.XmlElement;


public abstract class User {
	
	@XmlElement
	private int id;
	@XmlElement
	private String name;
	@XmlElement
	private String profession;

}
