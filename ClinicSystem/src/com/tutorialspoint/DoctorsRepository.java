package com.tutorialspoint;

import java.util.ArrayList;
import java.util.Date;

public class DoctorsRepository {
	
	private static DoctorsRepository instance = null;
	private int counter = 4;
    private ArrayList<Doctor> doctors = new ArrayList<Doctor>(){{
    	add(new Doctor(0, "Dorota Stolek", "Internista", 100));
    	add(new Doctor(1, "Elzunia Borek", "Internista", 102));
    	add(new Doctor(2, "Karol Glosnik", "Okulista", 203));
    	add(new Doctor(3, "Zbyszek Ufo", "Internista", 105));
    	add(new Doctor(4, "Mieciu Zatykacz", "Urolog", 201));
    }};
    
    protected DoctorsRepository(){
    };
    
    public static DoctorsRepository getInstance(){
    	if(instance == null){
    		instance = new DoctorsRepository();
    	}
    	return instance;
    }
    
    public ArrayList<Doctor> getDoctors(){
    	return this.doctors;
    }
    
    public Doctor getDoctor(int id){
    	for(Doctor x: doctors){
    		if(x.getId() == id){
    			return x;
    		}
    	}
    	return null;
    }
    
    public int addNewDoctor(int id, String name, String profession, int room){
    	Doctor doctor = new Doctor(id, name, profession, room);
    	for(Doctor x: doctors){
    		if(doctor.equals(x)){
    			return 0;
    		}
    	}
    	doctors.add(doctor);
    	return 1;
    }
    public void removeDoctor(int id){
    	for(int i=0; i<doctors.size(); i++){
    		if(doctors.get(i).getId() == id){
    			doctors.remove(i);
    			return;
    		}
    	}
    }
    public void updateDoctor(int id, String name, String profession, int room){
    	for(int i = 0; i< doctors.size(); i++){
    		if(doctors.get(i).getId() == id && doctors.get(i).getName() == name){
    			doctors.get(i).setProfession(profession);
    			doctors.get(i).setRoom(room);
    		}
    	}
    }
    public boolean isDoctor(int id, String name){
    	for(int i = 0; i<doctors.size(); i++){
    		if(doctors.get(i).getId() == id && doctors.get(i).getName().equals(name)){
    			return true;
    		}
    	}
    	return false;
    }


}
