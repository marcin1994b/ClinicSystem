package com.tutorialspoint;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/AppointmentService")
public class AppointmentService {
	
	PatientRepository patientRepository = PatientRepository.getInstance();
	DoctorsRepository doctorsRepository = DoctorsRepository.getInstance();
	
	@GET
	@Path("/patient/{patientid}")
	@Produces(MediaType.APPLICATION_XML)
	public List<Appointment> getPatientAppointment(@PathParam("patientid") int patientid){
		return patientRepository.getPatient(patientid).getAppointmentList().getAppointmentList();
	}
	
	@GET
	@Path("/doctor/{doctorid}")
	@Produces(MediaType.APPLICATION_XML)
	public List<Appointment> getDoctorAppointment(@PathParam("doctorid") int doctorid){
		return doctorsRepository.getDoctor(doctorid).getAppointmentList().getAppointmentList();
	}
	
	
	@PUT 
	@Path("/appointment")
	@Produces(MediaType.APPLICATION_XML)
	public String addNewDoctor(
			@FormParam("idDoctor") int idDoctor,
			@FormParam("nameDoctor") String nameDoctor,
			@FormParam("idPatient") int idPatient,
			@FormParam("namePatient") String namePatient,
			@FormParam("date") String date,
			@Context HttpServletResponse servletResponse) throws IOException{
		if(doctorsRepository.isDoctor(idDoctor, nameDoctor) == true){
			if(patientRepository.isPatient(idPatient, namePatient) == true){
				doctorsRepository.getDoctor(idDoctor).getAppointmentList().addAppointment(date, namePatient);
				patientRepository.getPatient(idPatient).getAppointmentList().addAppointment(date, nameDoctor);
				return "OK!";
			}
		}
		return "NIE OK!!!";
	}
	
	@DELETE
	@Path("/appointment/{type}/{name}/{id}/{date}")
	@Produces(MediaType.APPLICATION_XML)
	public String deleteAppointment(
			@PathParam("type") String type,
			@PathParam("name") String name,
			@PathParam("id") int id,
			@PathParam("date") String date){
		if(type.equals("doctor") == true){
			if(doctorsRepository.isDoctor(id, name) == true){
				doctorsRepository.getDoctor(id).getAppointmentList().removeAppointment(date);
				return "OK!";
			}
		}else if(type.equals("patient") == true){
			if(patientRepository.isPatient(id, name) == true){
				patientRepository.getPatient(id).getAppointmentList().removeAppointment(date);
				return "OK!";
			}
		}
		return "NIE OK!";
	}
	
	@OPTIONS
	@Path("/appointment")
	@Produces(MediaType.APPLICATION_XML)
	public String getSupportedOperations(){
		return "<operations>GET, PUT, DELETE</operations>";
	}
	   

}
