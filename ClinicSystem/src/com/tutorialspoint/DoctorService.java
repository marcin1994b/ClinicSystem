package com.tutorialspoint;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/DoctorService")
public class DoctorService {
	
	DoctorsRepository doctorsRepository = DoctorsRepository.getInstance();

   @GET
   @Path("/doctors")
   @Produces(MediaType.APPLICATION_XML)
   public List<Doctor> getDoctors(){
      return doctorsRepository.getDoctors();
   }
   
   @GET
   @Path("/doctors/{doctorid}")
   @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
   public Doctor getDoctor(@PathParam("doctorid") int doctorid){
      return doctorsRepository.getDoctor(doctorid);
   }
   
   @GET
   @Path("/doctors/{doctorid}/appointments")
   @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
   public List<Appointment> getAppointmentsList(@PathParam("doctorid") int doctorid){
      return doctorsRepository.getDoctor(doctorid).getAppointmentList().getAppointmentList();
   }
   
 //Content-Type: application/x-www-form-urlenscoded
   @PUT 
   @Path("/doctors")
   @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
   public List<Doctor> addNewDoctor(
	  @FormParam("id") int id,
      @FormParam("name") String name,
      @FormParam("profession") String profession,
      @FormParam("room") int room,
      @Context HttpServletResponse servletResponse) throws IOException{
      int result = doctorsRepository.addNewDoctor(id, name, profession, room);
      return doctorsRepository.getDoctors();
   }
   
   
   @POST
   @Path("/doctors")
   @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
   public List<Doctor> updateUser(@FormParam("id") int id,
      @FormParam("name") String name,
      @FormParam("profession") String profession,
      @FormParam("room") int room,
      @Context HttpServletResponse servletResponse) throws IOException{
      doctorsRepository.updateDoctor(id, name, profession, room);
      return doctorsRepository.getDoctors();
   }
   
   @DELETE
   @Path("/doctors/{doctorid}")
   @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
   public List<Doctor> deleteUser(@PathParam("doctorid") int doctorid){
      doctorsRepository.removeDoctor(doctorid);
      return doctorsRepository.getDoctors();
   }

   @OPTIONS
   @Path("/doctors")
   @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
   public String getSupportedOperations(){
      return "<operations>GET, PUT, POST, DELETE</operations>";
   }

}
