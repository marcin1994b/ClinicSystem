package com.tutorialspoint;

import java.util.ArrayList;

public class PatientRepository {
	
	private static PatientRepository instance = null;
	private int counter = 4;
    private ArrayList<Patient> patients = new ArrayList<Patient>(){{
    	add(new Patient(0, "Marcin Grzejnik", "mezczyzna", 18));
    	add(new Patient(1, "Karolina Kolek", "kobieta", 22));
    	add(new Patient(2, "Mietek Zbyt", "mezczyzna", 65));
    	add(new Patient(3, "Michal Uszko", "mezczyzna", 21));
    	add(new Patient(4, "Ania Bania", "kobieta", 45));
    }};
    
    public PatientRepository(){};
    
    public static PatientRepository getInstance(){
    	if(instance == null){
    		instance = new PatientRepository();
    	}
    	return instance;
    }
    
    public ArrayList<Patient> getPatients(){
    	return this.patients;
    }
    
    public Patient getPatient(int id){
    	for(Patient x: patients){
    		if(x.getId() == id){
    			return x;
    		}
    	}
    	return null;
    }
    
    public int addNewPatient(int id, String name, String gender, int age){
    	Patient patient = new Patient(id, name, gender, age);
    	for(Patient x: patients){
    		if(patient.equals(x)){
    			return 0;
    		}
    	}
    	patients.add(patient);
    	return 1;
    }
    public void removePatient(int id){
    	for(int i=0; i<patients.size(); i++){
    		if(patients.get(i).getId() == id){
    			patients.remove(i);
    			return;
    		}
    	}
    }
    
    public boolean isPatient(int id, String name){
    	for(int i = 0; i<patients.size(); i++){
    		if(patients.get(i).getId() == id && patients.get(i).getName().equals(name)){
    			return true;
    		}
    	}
    	return false;
    }


}
