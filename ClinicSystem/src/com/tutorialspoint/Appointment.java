package com.tutorialspoint;

import java.util.Date;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "appointment")
public class Appointment {
	
	private String date;
	private String person;
	
	public Appointment(){}
	
	public Appointment( String date, String person ){
		this.date = date;
		this.person = person;
	}
	
	@XmlElement
	public String getDate(){
		return this.date;
	}
	
	@XmlElement(name="doctor")
	public String getPerson(){
		return this.person;
	}
	
	public void setDate(String date){
		this.date = date;
	}
	
	public void setPerson(String person){
		this.person = person;
	}
	
	
	public boolean equals(Object object){
		if(object == null){
			return false;
		}else if(!(object instanceof Appointment)){
			return false;
		}else{
			Appointment appointment = (Appointment)object;
			if(date == appointment.getDate()){
				return true;
			}
			return false;
		}
	}

}
