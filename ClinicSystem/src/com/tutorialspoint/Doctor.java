package com.tutorialspoint;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "doctor")
public class Doctor extends User {
	
	private int id;
	private String name;
	private String profession;
	private int room;
	private AppointmentList appointmentList;
	
	
	public Doctor(){}
	
	public Doctor(int id, String name, String profession, int room){
		this.id = id;
		this.name = name;
		this.profession = profession;
		this.room = room;
		this.appointmentList = new AppointmentList();
	}
	
	
	public int getId(){
		return this.id;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getProfession(){
		return this.profession;
	}
	
	public int getRoom(){
		return this.room;
	}
	
	public AppointmentList getAppointmentList(){
		return this.appointmentList;
	}
	
	@XmlElement
	private void setId(int id){
		this.id = id;
	}
	
	@XmlElement
	private void setName(String name){
		this.name = name;
	}
	
	@XmlElement
	public void setProfession(String profession){
		this.profession = profession;
	}
	
	@XmlElement
	public void setRoom(int room){
		this.room = room;
	}
	
	public void setAppointment(String date, String person){
		appointmentList.addAppointment(date, person);
	}
	
	public boolean equals(Object object){
		if(object == null){
			return false;
		}else if(!(object instanceof Doctor)){
			return false;
		}else{
			Doctor doctor = (Doctor)object;
			if(id == doctor.getId() && name == doctor.getName()){
				return true;
			}
			return false;
		}
	}

}
