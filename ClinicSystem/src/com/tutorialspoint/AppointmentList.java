package com.tutorialspoint;

import java.util.ArrayList;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


public class AppointmentList {
	
	
	private ArrayList<Appointment> list;
	
	public AppointmentList(){
		list = new ArrayList<>();
	}
	
	public ArrayList<Appointment> getAppointmentList(){
		return this.list;
	}
	
	public void addAppointment(String date, String person){
		Appointment appointment = new Appointment(date, person);
		list.add(appointment);
	}
	
	public void removeAppointment(String date){
		for(int i = 0; i<list.size(); i++){
			if(list.get(i).getDate().equals(date)){
				list.remove(i);
				return;
			}
		}
	}
	
	

}
