package com.tutorialspoint;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/PatientService")
public class PatientService {
	
	PatientRepository patientRepository = PatientRepository.getInstance();

   @GET
   @Path("/patients")
   @Produces(MediaType.APPLICATION_XML)
   public List<Patient> getPatients(){
      return patientRepository.getPatients();
   }
   
   @GET
   @Path("/patients/{patientid}")
   @Produces(MediaType.APPLICATION_XML)
   public Patient getPatient(@PathParam("patientid") int patientid){
      return patientRepository.getPatient(patientid);
   }
   
   @PUT 
   @Path("/patients")
   @Produces(MediaType.APPLICATION_XML)
   public List<Patient> addNewDoctor(
	  @FormParam("id") int id,
      @FormParam("name") String name,
      @FormParam("gender") String gender,
      @FormParam("age") int age,
      @Context HttpServletResponse servletResponse) throws IOException{
      int result = patientRepository.addNewPatient(id, name, gender, age);
      return patientRepository.getPatients();
   }
   
   
   @DELETE
   @Path("/patients/{patientid}")
   @Produces(MediaType.APPLICATION_XML)
   public List<Patient> deletePatient(@PathParam("patientid") int patientid){
	   patientRepository.removePatient(patientid);
      return patientRepository.getPatients();
   }

   @OPTIONS
   @Path("/patients")
   @Produces(MediaType.APPLICATION_XML)
   public String getSupportedOperations(){
      return "<operations>GET, PUT, POST</operations>";
   }

}
